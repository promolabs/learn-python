#!/usr/bin/env python

body = {
    "id": "57d7c7103f1eb43cd145df3e",
    "quantity": 3,
    "variant_id": "34108",
    "custom_options": {
        "test": {
            "a": "1",
            "b": "2",
            "c": "3"
        }
    }
}


def create_form(body, action="/", method="post", id="auto_post", enctype="application/json", style="display:none"):
    return "<form action='{}' method='{}' id='{}' enctype='{}' style='{}'>{}</form>{}" \
        .format(action, method, id,
                enctype, style,
                add_inputs(body),
                submit_form(id))


def submit_form(id):
    return "<script type='text/javascript'>document.getElementById('{}').submit()</script>".format(id)


def add_inputs(body, prefix=""):
    result = ""
    for index in body:
        name = index
        value = body[index]
        if len(prefix):
            name = "{}[{}]".format(prefix, name)
        if type(value) in (tuple, list, dict):
            result += add_inputs(value, name)
        else:
            result += "<input name='{}' value='{}' type='hidden'>".format(name, value)
    return result


print(create_form(body))
